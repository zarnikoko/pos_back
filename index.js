const express = require('express')
const bodyParser = require('body-parser')
const  app = express()
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors');

//import routes
const usersRoute = require('./routes/users')
const authRoute = require('./routes/auth')
const permissionRoute = require('./routes/permissions')
const roleRoute = require('./routes/roles')


dotenv.config()

//Middleware
app.use(cors());
app.use(bodyParser.json());

//Route Middleware
app.use('/api/users',usersRoute)
app.use('/api',authRoute)
app.use('/api/permissions',permissionRoute)
app.use('/api/roles',roleRoute)


//connect to DB
mongoose.connect(
process.env.DB_CONNECT
,{ useUnifiedTopology: true,useNewUrlParser: true}
,()=>console.log("DB Connected!")
)

app.listen('5000',()=>console.log("Server up and running at port 5000"))