const jwt = require('jsonwebtoken')

const verifyToken= function (req,res,next) {
    const token = req.header('Access_Token')
    if(!token) return res.status(401).send({
        name : 'No Valid Token Error'
        ,message:'Access Denied'
    })
    try{
        const verified = jwt.verify(token,process.env.ACCESS_TOKEN_SECRET)
        req.user = verified
        next()
    }catch(err){
        return res.status(401).send(err)
    }
}

module.exports = verifyToken