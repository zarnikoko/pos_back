const Joi = require('@hapi/joi')

const userValidation = data => {
    const schema = Joi.object({
        name : Joi.string()
            .min(2)
            .required()
        ,
        password : Joi.string()
            .min(4)
            .required()
        ,
        role_id : Joi.string()
            .max(255)
            .required()
    })
    return schema.validate(data)
}

const signinValidation = data => {
    const schema = Joi.object({
        name : Joi.string()
            .min(2)
            .required()
        ,
        password : Joi.string()
            .min(4)
            .required()
    })
    return schema.validate(data)
}

const roleValidation = data => {
    const schema = Joi.object({
        "Role's Name" : Joi.string()
            .max(255)
            .required()
        ,
        "Permissions" : Joi.array()
            .min(1)
            .required()
        
    })
    return schema.validate(data)
}

module.exports = {
    userValidation,
    signinValidation,
    roleValidation
}
