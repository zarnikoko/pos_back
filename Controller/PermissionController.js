const Permission = require('../Model/Permission.Model')

const index = async(req,res) => {
    Permission.find({}, (err, permissions) => {
        if (err) {
          return res.status(500).json({ err });
        }
        return res.json({ permissions });
    }).exec();
}

module.exports = {
    index
}