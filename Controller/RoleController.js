const Role = require('../Model/Role.Model')
const { roleValidation } = require('../Common/validation');

const index = async(req,res) => {
    let {sortBy,sortType,name} = req.query
    if(sortBy===undefined) sortBy = 'name' 
    if(sortType===undefined) sortType = 'asc'
    if(typeof name !== 'string') name = '';
    let exp = {}
    if(name!=='') exp = {
        "name": { 
            $regex: '.*' + name + '.*',
            $options:['-i']
        }
    }
    Role.find(exp,(err,roles)=>{
        if(err){
            return res.status(500).json(err)
        }
        return res.status(200).json({roles})
    }).sort([[sortBy,sortType]])
}

const store = async (req,res) => {
    const {error} = roleValidation({
        "Role's Name" : req.body.name,
        "Permissions":req.body.permissions
    });
    if(error) {
        return res.status(400).json({
            message:error.details[0].message,
            field:error.details[0].context.label,
            type:error.details[0].type,
        })
    }
    const roleExist = await Role.findOne({name:req.body.name})
    if(roleExist){
        return res.status(400).json({
            message:"Role' Name is already exist! ",
            field:"Role's Name",
            type:"unique"
        })
    }
    const role = new Role({
        name : req.body.name,
        permissions : req.body.permissions,
        createdUser : req.user._id
    })
    try{
        let savedRole = await role.save();
        res.status(201).json({type:'store',role:savedRole});
    }catch(err){
        res.status(500).send(err);
    }
}

const edit =  async (req,res) => {
    const {_id} = req.params;
    try{
        Role.findOne({_id:_id},(err,role)=>{
            if(err) return res.status(500).json(err)
            return res.status(200).json({role})
        })
    }catch(err){
        res.status(500).send(err);
    }
    
}

const update = async (req,res) => {
    const {_id} = req.params
    const {error} = roleValidation({
        "Role's Name" : req.body.name,
        "Permissions":req.body.permissions
    });
    if(error) {
        return res.status(400).json({
            message:error.details[0].message,
            field:error.details[0].context.label,
            type:error.details[0].type,
        })
    }
    const roleExist = await Role.findOne({$and: [{name: req.body.name}, {_id: {$ne:_id}}]})
    if(roleExist){
        return res.status(400).json({
            message:"Role' Name is already exist! ",
            field:"Role's Name",
            type:"unique"
        })
    }
    const role = {
        name : req.body.name,
        permissions : req.body.permissions,
        updatedUser : req.user._id
    };

    try{
        Role.updateOne({_id:_id},role,function(error,doc,result){
            if (error) res.status(500).json({error});
            res.status(200).json({type:'update',updatedRole:doc});
        })
    }catch(err){
        res.status(400).send(err);
    }
}

const destroy =  async (req,res) => {
    const {_id} = req.params;
    
    try{
        Role.findOneAndDelete({_id:_id}, function( error, doc,result) {
            // it will be already removed, but doc is what you need:
            if (error) res.status(500).send(error);
            res.status(200).json({type:'destroy',deleted_id:doc.id});
        });
    }catch(err){
        res.status(500).send(err);
    }
}

module.exports = {
    index,
    store,
    destroy,
    edit,
    update
}