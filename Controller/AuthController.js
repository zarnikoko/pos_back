const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require("../Model/User.Model");
const { signinValidation } = require("../Common/validation");
const Token = require('../Model/Token.Model');
const Role = require('../Model/Role.Model');

const signin = async(req,res) => {
    //validate before sign in a user
    const {error} = signinValidation({
        "name" : req.body.username,
        "password":req.body.password
    });
    if(error) return res.status(400).json({message:error.details[0].message,type:error.details[0].context.label})
    
    // checking username exist 
    let user = await User.findOne({name:req.body.username})
    if(!user) return res.status(400).json({message:'User doesn\'t exist!',type:"name"})

    // checking password
    const validPass = await bcrypt.compare(req.body.password,user.password)
    if(!validPass) return res.status(400).json({message:'User name and password do not match',type:"password"})
    // create and assign tokens
    let role = await Role.findById(user.role_id);
    const payLoad = {
        _id:user._id
        ,name:user.name
        ,permissions:role.permissions
    };
    const access_token = await user.createAccessToken();

    const refresh_token = await user.createRefreshToken();

    res.status(202).json({
        access_token:access_token,
        refresh_token:refresh_token,
        user:payLoad
    })
}

const signout = async(req,res) => {
    try {
        //get refreshToken
        const refreshToken  = req.header('Refresh_Token');
        //send error if no refreshToken is sent
        if (!refreshToken) {
            return res.status(403).json({ error: "Token missing!" });
        } else {
            Token.findOneAndDelete({token:refreshToken}, function( error,doc,result) {
            // it will be already removed, but doc is what you need:
            if (error) res.status(500).send(error);
            res.status(200).json({message:'success'});
        });
        }
    }   catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal Server Error!" });
    }
}

const checkToken = async (req,res) => {
    res.status(202).json(
        {
            message:'Accepted',
            user:{
                _id : req.user._id,
                name : req.user.name,
                permissions: req.user.permissions
            }
        }
    );
}

const generateRefreshToken = async (req, res) => {
    try {
        //get refreshToken
        const refreshToken  = req.header('Refresh_Token');
        //send error if no refreshToken is sent
        if (!refreshToken) {
            return res.status(403).json({ error: "Access denied,token missing!" });
        } else {
            //query for the token to check if it is valid:
            const tokenDoc = await Token.findOne({ token: refreshToken });
            //send error if no token found:
            if (!tokenDoc) {
            return res.status(401).json({ error: "Token expired!" });
            } else {
            //extract payload from refresh token and generate a new access token and send it
            const payload = jwt.verify(tokenDoc.token, process.env.REFRESH_TOKEN_SECRET);
            const { _id, name, permissions } = payload;
            const accessToken = jwt.sign({...{ _id, name, permissions }}, process.env.ACCESS_TOKEN_SECRET, {
                expiresIn: "10m",
            });
            return res.status(202).json({ 
                user :{...{ _id, name, permissions }},
                accessToken:accessToken 
            });
            }
        }
    }   catch (error) {
        console.error(error);
        return res.status(500).json({ error: "Internal Server Error!" });
    }
};

module.exports = {
    signin,
    checkToken,
    generateRefreshToken,
    signout
}