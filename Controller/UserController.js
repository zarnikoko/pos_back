const bcrypt = require('bcryptjs')
const User = require('../Model/User.Model');
const { registValidation } = require('../Common/validation');

const store = async (req,res) =>{
    
    //validate before save a user
    const {error} = userValidation({
        "name" : req.body.username,
        "password":req.body.password,
        "role_id":req.body.role_id
    });
    if(error) return res.status(400).json({message:error.details[0].message})
    
    // checking username exist 
    const usernameExist = await User.findOne({name:req.body.username})
    if(usernameExist) return res.status(400).json({message:'User name already exist!'})

    // const d = new Date(0);
    // d.setUTCSeconds(1607191622);
    const user = new User({
        name : req.body.username,
        password : req.body.password,
        role_id:req.body.role_id,
        // expiredAt:d
    })
    try{
        let savedUser = await user.save();
        savedUser = savedUser.toObject();
        delete savedUser.password;
        res.status(202).json({type:'store',user:savedUser});
    }catch(err){
        res.status(400).send(err);
    }
}

module.exports = {
    store
}
