const router = require('express').Router()
const verifyToken = require('../Common/verifyToken')
const PermissionController = require('../Controller/PermissionController')

router.get('/',verifyToken,PermissionController.index);

module.exports = router