const router = require('express').Router()
const checkPermission = require('../Common/checkPermission');
const verifyToken = require('../Common/verifyToken')
const RoleController = require('../Controller/RoleController')

router.post('/',verifyToken,RoleController.store);
router.get('/',verifyToken,checkPermission.role,RoleController.index);
router.delete('/:_id',verifyToken,RoleController.destroy);
router.get('/:_id/edit',verifyToken,RoleController.edit);
router.patch('/:_id',verifyToken,RoleController.update);

module.exports = router