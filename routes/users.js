const router = require('express').Router()
const UserController = require('../Controller/UserController');
const verifyToken = require('../Common/verifyToken')

router.post('/store',verifyToken,UserController.store);

// router.patch('/update',verifyToken,async(req,res)=>{
//     //validate before save a user
//     const {error} = registValidation({
//         "name" : req.body.name,
//         "email": req.body.email,
//         "type" : req.body.type,
//     });
//     if(error) return res.status(400).json({message:error.details[0].message})
    
//     // checking email exist 
//     const emailExist = await User.findOne({$and: [{email: req.body.email}, {_id: {$ne:req.body._id}}]})
//     if(emailExist) return res.status(400).json({message:'Email already exist!'})
//     let updatedUser = {
//         "name" : req.body.name,
//         "email": req.body.email,
//         "type" : req.body.type  
//     };
//     if(req.body.resetPsw){
//         const salt = await bcrypt.genSalt(10)
//         const hashedPassword = await bcrypt.hash("12345678",salt);
//         updatedUser.password = hashedPassword;
//     }
//     try{
//         User.findByIdAndUpdate(req.body._id,updatedUser,function(error,doc,result){
//             if (error) res.status(500).send(error);
//             delete req.body.password;
//             res.json({type:'update',success:req.body});
//         })
//     }catch(err){
//         res.status(400).send(err);
//     }
// });

// router.delete('/delete',verifyToken,async(req,res)=>{
//     try{
//         User.findOneAndDelete({_id:req.body.id}, function( error, doc,result) {
//             // it will be already removed, but doc is what you need:
//             if (error) res.status(500).send(error);
//             res.json({success:doc.id});
//         });
//     }catch(err){
//         res.status(400).send(err);
//     }
// });

// //get all users
// router.get('/',verifyToken,async(req,res)=>{
//     let currentPage = Number(req.query.currentPage);
//     let limit = Number(req.query.limit);
//     let searchBy = req.query.searchBy.toLowerCase();
//     let searchWords = req.query.searchWords;
//     console.log(req.query);
//     try{
//         let data = [];
//         data = await getUsers(currentPage,limit,searchBy,searchWords);
//         const pages = Math.ceil(data['counts']/limit);
//         if(pages<currentPage && pages!==0){
//             currentPage -=1;
//             data = await getUsers(currentPage,limit,searchBy,searchWords);
//         }
//         res.json({users:data['users'],counts:data['counts'],pages:pages,currentPage:currentPage,limit:limit,searchBy:searchBy,searchWords:searchWords});
//     }catch(error){
//         res.status(400).json({message:error});
//     }
// });

// const getUsers = async (page,limit,searchBy,searchWords) => {
//     let data = [];
//     let count = 0;
//     let users = {};
//     if(searchWords===""){
//         counts = await User.countDocuments();
//         users = await User.find({},{password:0})
//         .limit(limit)
//         .skip(limit*(page-1))
//         .sort([['updated_at',-1]]);
//     }else{
//         counts = await User.countDocuments(
//             {   
//                 [searchBy]  :  { 
//                     $regex: '.*' + searchWords + '.*',
//                     $options: ['-i'] 
//                 }
//             });
//         users = await User.find(
//         {
//             [searchBy]: { 
//                 $regex: '.*' + searchWords + '.*',
//                 $options:['-i']
//             }
//         },
//         {   
//             password:0
//         })
//         .limit(limit)
//         .skip(limit*(page-1))
//         .sort([['updated_at',-1]]);
//     }
//     data['counts'] = counts;
//     data['users'] = users;
//     return data;
// }

module.exports = router;