const router = require('express').Router()
const verifyToken = require('../Common/verifyToken')
const AuthController = require('../Controller/AuthController')

router.post('/signin',AuthController.signin)

router.delete('/signout',verifyToken,AuthController.signout)

router.post('/generateRefreshToken',AuthController.generateRefreshToken);

router.post('/signout',AuthController.signout);

module.exports = router;
