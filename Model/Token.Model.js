const mongoose = require('mongoose')

const tokenSchema = new mongoose.Schema({
    user_id:{
        type:String,
        required:true
    },
    token:{
        type:String,
        required:true,
    },
    createdAt: { type: Date, expires: 86400 , default: Date.now }
})

module.exports = mongoose.model("Token",tokenSchema)