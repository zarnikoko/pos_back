const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        max:255,
        unique:true
    },
    permissions: [
        {
            name:{type:String},
            path:{type:String},
            display:{type:String},
            parent:{type:String},
            sidebar:{type:Boolean,default:true}
        }
    ],
    countOfPermissions:{
        type:Number
    },
    createdUser: {
        type:String
    },
    updatedUser: {
        type:String
    }
},
    { timestamps: { createdAt:'created_at',updatedAt:'updated_at' } }
)

roleSchema.pre("save",async function(next){
    try{
        this.countOfPermissions = this.permissions.filter((p)=>p.path !== '').length
    }catch(error){
        console.error(error);
    }
    return next()
})

roleSchema.pre("updateOne",async function(next){
    try{
        this._update.countOfPermissions = this._update.permissions.filter((p)=>p.path !== '').length
    }catch(error){
        console.error(error);
    }
    return next()
})

module.exports = mongoose.model("Role",roleSchema)