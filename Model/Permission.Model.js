const mongoose = require('mongoose')

const permissionSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        max:255,
    },
    path:{
        type:String,
        max:255,
    },
    display:{
        type:String,
        required:true,
        max:255,
    },
    parent:{
        type:String,
        max:255,
    },
    sidebar:{
        type:Boolean,
        default:true
    }
})

module.exports = mongoose.model("Permission",permissionSchema)