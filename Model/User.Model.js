const mongoose = require('mongoose')
const jwt = require("jsonwebtoken")
const bcrypt = require("bcryptjs")
const Token = require('./Token.Model')
const { date, boolean } = require('@hapi/joi');
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        min:2,
        max:255,
    },
    password:{
        type:String,
        required:true,
        max:1024,
        min:8
    },
    role_id : {
        type:String,
        required:true,
        max:255
    },
    // expiredAt: { type: Date, expires: '1m'} auto delete after expire time
}, 
{ timestamps: { createdAt:'created_at',updatedAt:'updated_at' } }
)

userSchema.methods = {
    createAccessToken: async function () {
        try {
            let { _id, name , permissions } = this;
            let accessToken = jwt.sign(
                {   ...{ _id, name, permissions } },
                    process.env.ACCESS_TOKEN_SECRET,
                {
                    expiresIn: "1m",
                }
            );
            return accessToken;
        } catch (error) {
            console.error(error);
            return;
        }
    },
    createRefreshToken: async function () {
        try {
            let { _id, name, permissions } = this;
            let refreshToken = jwt.sign(
                {   ...{ _id, name, permissions } },
                    process.env.REFRESH_TOKEN_SECRET,
                {
                    expiresIn: "1d",
                }
            );
            await new Token({ 
                user_id: _id,
                token: refreshToken,
                date : Date.now()
            }).save();
            return refreshToken;
        } catch (error) {
            console.error(error);
            return;
        }
    }
};

//pre save hook to hash password before saving user into the database:
userSchema.pre("save", async function (next) {
    try {
        let salt = await bcrypt.genSalt(12); // generate hash salt of 12 rounds
        let hashedPassword = await bcrypt.hash(this.password, salt); // hash the current user's password
        this.password = hashedPassword;
    } catch (error) {
        console.error(error);
    }
    return next();
});

module.exports = mongoose.model("User",userSchema);